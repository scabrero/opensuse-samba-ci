FROM opensuse/tumbleweed:latest

LABEL maintainer="Samuel Cabrero <scabrero@suse.de>"
LABEL description="OpenSUSE Tumbleweed container for samba CI"

RUN zypper --non-interactive addrepo --priority 25 \
  https://download.opensuse.org/repositories/home:/scabrero:/samba-ci-tumbleweed/openSUSE_Tumbleweed/home:scabrero:samba-ci-tumbleweed.repo
RUN zypper --non-interactive --gpg-auto-import-keys refresh

# Required to build samba
RUN zypper --non-interactive install --no-recommends \
	acl			\
	attr			\
	autoconf		\
	bash			\
	cups-devel		\
	cyrus-sasl-devel	\
	cracklib-devel		\
	dbus-1-devel		\
	docbook-xsl-stylesheets	\
	e2fsprogs-devel		\
	fdupes			\
	gcc			\
	cpp			\
	gdbm-devel		\
	glibc-devel		\
	glibc-locale		\
	gzip			\
	jq			\
	keyutils-devel		\
	krb5-devel		\
	krb5-server		\
	krb5-plugin-preauth-pkinit \
	libacl-devel		\
	libarchive-devel	\
	libattr-devel		\
	libavahi-devel		\
	libblkid-devel		\
	libcephfs-devel		\
	libgnutls-devel		\
	libjansson-devel	\
	libnettle-devel		\
	libnsl-devel		\
	libopenssl-devel	\
	libpcp-devel		\
	librados-devel		\
	libtasn1-devel		\
	libtasn1-tools		\
	libtirpc-devel		\
	libunwind-devel		\
	liburing-devel		\
	libuuid-devel		\
	libxslt			\
	libxslt-tools		\
	make			\
	ncurses-devel		\
	openldap2-devel		\
	patch			\
	pam-devel		\
	perl-JSON		\
	pkg-config		\
	popt-devel		\
	pwdutils		\
	readline-devel		\
	systemd-devel		\
	psmisc			\
	python311-devel		\
	python311-xml		\
	python311-iso8601	\
	python311-cryptography	\
	python311-pyasn1	\
	python311-requests	\
	rpcgen			\
	tack

RUN zypper --non-interactive install --no-recommends \
	ccache			\
	bind-utils		\
	curl			\
	libgpgme-devel		\
	lmdb-devel		\
	rng-tools		\
	libbsd-devel		\
	libcap-devel		\
	flex			\
	libaio-devel

# Required to run the test suite
RUN zypper --non-interactive install --no-recommends --allow-vendor-change \
	diffutils		\
	gdb			\
	git-core		\
	hostname		\
	lmdb			\
	lsb-release		\
	system-user-nobody	\
	perl-Parse-Yapp		\
	python311-chardet	\
	python311-gpg		\
	python311-Markdown	\
	python311-pycryptodome	\
	python311-dnspython	\
	python311-setproctitle	\
	rsync			\
	tar			\
	valgrind-devel		\
	which			\
	chrpath			\
	libicu-devel		\
	bison			\
	libgnutls30-hmac

# Utils for development
RUN zypper --non-interactive install --no-recommends \
	iproute2		\
	krb5-client		\
	screen			\
	sudo			\
	vim			\
	valgrind		\
	iproute2

ADD ./krb5.sh /etc/profile.d/krb5.sh

RUN useradd --shell /bin/bash --create-home --home-dir /home/samba samba
RUN echo "samba ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/samba

RUN mkdir /memdisk && chown samba /memdisk && chgrp users /memdisk

SHELL ["/bin/bash", "--login", "-c"]

USER samba

WORKDIR /home/samba

CMD ["/bin/bash", "--login"]

ENV USER "samba"
ENV LANG "en_US.UTF-8"
ENV LANGUAGE "en_US:en"
ENV LC_ALL "en_US.UTF-8"
