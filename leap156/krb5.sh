#!/bin/bash

for i in $(ls /usr/lib/mit/bin); do
	ln -srf /usr/lib/mit/bin/$i /usr/bin/$i
done

for i in $(ls /usr/lib/mit/sbin); do
	ln -srf /usr/lib/mit/sbin/$i /usr/sbin/$i
done
