# OpenSUSE based images for samba CI

This project builds OpenSUSE based docker images suitable to run the samba
test suite. They are also useful for development or debugging.
